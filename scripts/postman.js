import http from "k6/http";
import { check } from "k6";
import { group } from "k6";

// Can override the API endpoint byt setting API_HOST env var
const API_HOST = __ENV.API_HOST || `https://postman-echo.com`;

export let options = {
  maxRedirects: 4,
  stages: [
    { duration: "10s", target: 10 }, // 10 seconds of 10 VUs
    { duration: "20s", target: 20 }, // 20 seconds of 20 VUs
    { duration: "45s", target: 50 }, // 45 seconds of 50 VUs
    { duration: "3m", target: 80 }, // 3 minutes of 80 VUs
    { duration: "10s", target: 0 } // 10 ramping down to zero VUs
  ],
  thresholds: {
    "failed requests": ["rate < 0.1"], // Check total failed requests
    http_req_duration: ["avg < 200"] // Check average response times under 500ms
  }
};

export default function() {
  // Group for API GET requests
  group("API GET Existing colour", function() {
    let url = `${API_HOST}/get?colour=blue`;

    let res = http.get(url);

    // Validate result with a check function
    check(res, {
      "Status was 200": r => r.status === 200,
      "Returned colour is correct": r =>
        JSON.parse(r.body).args.colour === "blue"
    });
  });

  // Group for API POST requests
  group("API POST New colour", function() {
    let url = `${API_HOST}/post`;
    let payload = JSON.stringify({
      colour: "green"
    });

    let res = http.post(url, payload, {
      headers: { "Content-Type": "application/json" }
    });

    // Validate result with a check function
    check(res, {
      "Status was 200": r => r.status === 200,
      "Response has data object": r =>
        typeof JSON.parse(r.body).data === "object",
      "Response has correct colour": r =>
        JSON.parse(r.body).data.colour === "green"
    });
  });
}
