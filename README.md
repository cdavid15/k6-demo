# K6 Performance Testing Sample Project

The repository has a single performance test script to show how basic usage of [K6](https://k6.io) locally using the executable and docker as well the configuration to trigger the scripts as part of a GitLab CI pipeline.

## Running K6 Locally

To execute the K6 script locally the following commands can be used:

```shell
### ensure the volume path is mapped as required
docker run -i -v d:/code/k6/k6-demo/scripts:/scripts loadimpact/k6 run  /scripts/postman.js
```

Or if you have K6 installed on the machine:

```
  k6 run --out influxdb=http://localhost:8086/k6 ./scripts/postman.js
```

## GitLab CI Configuration

Example configuration has been provided to show to execute the scripts through GitLab CI which is as follows:

```yml
loadtest:
  stage: test
  image:
    name: loadimpact/k6:latest
    entrypoint: [""]
  script: k6 run ./scripts/postman.js

loadtest:coverage:
  stage: test
  image:
    name: loadimpact/k6:latest
    entrypoint: [""]
  script: k6 run --out influxdb=http://<<ip/host>>:8086/k6 ./scripts/postman.js
  when: manual
```

## Visually results in Grafana

Configuration has also been provided to output the results to an InfluxDB and rendered onto a Grafana Dashboard to visualise the results.

To execute the script locally with output send to InfluxDB execute the following commands:

```shell
    ### Starts up the influx and grafana services in daemon mode
    docker-compose up -d influxdb grafana

    ### if using the K6 docker image
    docker run -i -v d:/code/k6/k6-demo/scripts:/scripts loadimpact/k6 run --out influxdb=http://10.0.75.1:8086/k6  /scripts/postman.js

    ### if using the K6 executable
    k6 run --out influxdb=http://localhost:8086/k6 ./scripts/postman.js
```

Further info can be found on the [K6 docs site](https://docs.k6.io/docs/influxdb-grafana).
